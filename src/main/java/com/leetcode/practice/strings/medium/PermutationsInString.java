package com.leetcode.practice.strings.medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author swghan
 *Given two strings s1 and s2, write a function to return true 
 *if s2 contains the permutation of s1. In other words, 
 *one of the first string's permutations is the substring of 
 *the second string.

Example 1:

Input: s1 = "ab" s2 = "eidbaooo"
Output: True
Explanation: s2 contains one permutation of s1 ("ba").
Example 2:

Input:s1= "ab" s2 = "eidboaoo"
Output: False
 

Note:

The input strings only contain lower case letters.
The length of both given strings is in range [1, 10,000].
 */


public class PermutationsInString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(checkInclusion("adc", "dcda"));
	}
	
	public static boolean checkInclusion1(String s1, String s2) {
        Map<Character, Integer> lookup = new HashMap<>();
        int missing = s1.length();
        
        for(Character c : s1.toCharArray()) {
        	if(lookup.containsKey(c)) {
        		lookup.computeIfPresent(c, (key, value) -> ++value);
        	}
        	else {
        		lookup.put(c, 1);
        	}
        }
        int n = s2.length();
        int i=0;
        int j=0;
        while(i<n || j<n){
        	if(lookup.containsKey(s2.charAt(i)) 
        			&& lookup.get(s2.charAt(i))>0){
        		lookup.computeIfPresent(s2.charAt(i), (key, value) 
        				-> -- value);
        		missing --;
        		if(missing == 0) {
        			return true;
        		}
        	}
        }
        
        return missing ==0;
    }
	
	public static boolean checkInclusion(String s1, String s2) {
        Map<Character, Integer> lookup = new HashMap<>();
        int missing = s1.length();
        boolean foundMode = false;
        ArrayList<Character> observed = new ArrayList<>();
        for(Character c : s1.toCharArray()) {
        	if(lookup.containsKey(c)) {
        		lookup.computeIfPresent(c, (key, value) -> ++value);
        	}
        	else {
        		lookup.put(c, 1);
        	}
        }
        
        for(int i=0; i < s2.length(); i++) {
        	if(lookup.containsKey(s2.charAt(i)) 
        			&& lookup.get(s2.charAt(i))>0){
        		foundMode = true;
        		
        		lookup.computeIfPresent(s2.charAt(i), (key, value) 
        				-> -- value);
        		observed.add(s2.charAt(i));
        		missing --;
        		if(missing == 0) {
        			return true;
        		}
        	}
        	else {
        		for(Character c : observed) {
        			lookup.computeIfPresent(c, (key, value) 
            				-> ++ value);
        		}
        		observed = new ArrayList<Character>();
        		missing = s1.length();
        		foundMode = false;
        	}
        }
        
        return missing ==0;
    }

}
