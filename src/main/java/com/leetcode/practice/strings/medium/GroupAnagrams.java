/*

Given an array of strings, group anagrams together.

Example:

Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
Output:
[
  ["ate","eat","tea"],
  ["nat","tan"],
  ["bat"]
]
Note:

All inputs will be in lowercase.
The order of your output does not matter.
 
*/

package com.leetcode.practice.strings.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupAnagrams {
	public static void main(String args[]) {
		List<List<String>> result = groupAnagrams1(new String[] { "eat", "tea", "tan", "ate", "nat", "bat" });
		for (List<String> list : result) {
			for (String ans : list)
				System.out.print(ans + ", ");
			System.out.println();
		}
	}

	public static List<List<String>> groupAnagrams(String[] strs) {
		List<List<String>> result = new ArrayList<List<String>>();
		if (strs == null || strs.length == 0) {
			return result;
		}
		Map<String, List<String>> lookup = new HashMap();
		char[] arr = null;
		List<String> value = null;
		String sortedKey = null;
		for (String str : strs) {
			arr = str.toCharArray();
			Arrays.sort(arr);
			sortedKey = String.valueOf(arr);
			if (lookup.containsKey(sortedKey)) {
				value = lookup.get(sortedKey);
			} else {
				value = new ArrayList<String>();
			}
			value.add(str);
			lookup.put(sortedKey, value);
		}

		for (List<String> entry : lookup.values()) {
			result.add(entry);
		}

		return result;
	}

	public static List<List<String>> groupAnagrams1(String[] strs) {
		int[] prime = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
				103 };

		List<List<String>> res = new ArrayList<>();
		HashMap<Integer, Integer> map = new HashMap<>();
		for (String s : strs) {
			int key = 1;
			for (char c : s.toCharArray()) {
				key *= prime[c - 'a'];
			}
			List<String> t;
			if (map.containsKey(key)) {
				t = res.get(map.get(key));
			} else {
				t = new ArrayList<>();
				res.add(t);
				map.put(key, res.size() - 1);
			}
			t.add(s);
		}
		return res;
	}
}
