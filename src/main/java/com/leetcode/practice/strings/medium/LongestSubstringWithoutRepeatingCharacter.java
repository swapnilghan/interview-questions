package com.leetcode.practice.strings.medium;

/*

Given a string, find the length of the longest substring without 
repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
Note that the answer must be a substring, "pwke" 
is a subsequence and 
not a substring.

*/

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacter {

	public static void main(String[] args) {
		System.out.println(lengthOfLongestSubstring(" "));
	}
    public static int lengthOfLongestSubstring_naive(String s) {
    	int ans = 0;
    	for(int i = 0; i < s.length(); i++) {
    		for(int j = i+1; j <= s.length(); j++) {
    			if(isUnique(s.substring(i, j))) {
    				ans = Math.max(ans, (j-i));
    			}
    		}
    	}
    	return ans;
    }
    
    public static boolean isUnique(String subStr) {
    	boolean unique = true;
    	Set<Character> lookUp = new HashSet<Character>();
    	
    	for(int i=0; i < subStr.length(); i++) {
    		if(!lookUp.add(subStr.charAt(i)))
    			return false;
    	}
    	return unique;
    }
    
    public static int lengthOfLongestSubstring(String s) {
        int n = s.length();
        Set<Character> set = new HashSet<>();
        int ans = 0, i = 0, j = 0;
        while (i < n && j < n) {
            // try to extend the range [i, j]
            if (!set.contains(s.charAt(j))){
                set.add(s.charAt(j++));
                ans = Math.max(ans, j - i);
            }
            else {
                set.remove(s.charAt(i++));
            }
        }
        return ans;
    }
    
}
