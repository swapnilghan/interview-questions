/*

Given a string S and a string T, find the minimum window in S 
which will contain all the characters in T in complexity O(n).

Example:

Input: S = "ADOBECODEBANC", T = "ABC"
Output: "BANC"
Note:

If there is no such window in S that covers all characters in T, 
return the empty string "".
If there is such window, you are guaranteed that there will 
always be only one unique minimum window in S.

*/

package com.leetcode.practice.strings.hard;

import java.util.HashMap;
import java.util.Map;

public class MinimumWindowSubstring {

	public static void main(String[] args) {
		System.out.println(minWindow("AA", "AA"));
	}
	
	private static String minWindow_naive(String s, String t) {
		String subStr = "";
		String answer = "";
		int minLen = Integer.MAX_VALUE;
		boolean allPresent = true;
		char[] arr = t.toCharArray();
        for(int i=0; i < s.length(); i++) {
        	for(int j=i+t.length(); j <=s.length(); j++) {
        		subStr = s.substring(i, j);
        		allPresent = true;
        		for(int k = 0; k < arr.length; k++) {
        			if(subStr.indexOf(arr[k]) < 0)
        				allPresent = false;
        		}
        		if(allPresent && subStr.length() < minLen) {
        			answer = subStr;
        			minLen = subStr.length();
        		}
        			
        	}
        }
		return answer;
    }
	
	private static String minWindow(String s, String t) {
		int iStart = 0;
		int iEnd = s.length();
		int missing = t.length();
		Map<Character, Integer> map = new HashMap<>();
		int slow = 0;
		String answer = "";
		
		if(t.length() > s.length()) {
			return answer;
		}
		for(int i =0; i < missing; i++) {
			if(map.containsKey(t.charAt(i)))
				map.computeIfPresent(t.charAt(i), (key, value) -> --value);
			else
				map.put(t.charAt(i), 0);
		}
		
        for(int fast = 0; fast < s.length(); fast++) {
        	if(t.indexOf(s.charAt(fast))> -1) {
        		if(map.get(s.charAt(fast)) <= 0) {
        			missing--;
        			map.compute(s.charAt(fast), (key, val) -> ++val);
        		}
        		else {
        			map.compute(s.charAt(fast), (key, val) -> ++val);
        		}
        	}
        	
        	while(missing == 0) {
        		if((fast-slow) < (iEnd-iStart)) {
        			iEnd = fast;
        			iStart = slow;
        			answer = s.substring(iStart, ++iEnd);
        		}
        		if(t.indexOf(s.charAt(slow))> -1) {
        			if(map.get(s.charAt(slow)) == 1) {
            			missing++;
            		}
            		map.compute(s.charAt(slow), (key, val) -> --val);
            		
        		}
        		slow++;
        	}
        }
		return answer;
    }
}
