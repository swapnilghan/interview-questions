/*
Convert integer to string representation to a given base

e.g input (10, 9) --> "11"
e.g input (23, 10) --> "23"
 
*/

package com.leetcode.practice.strings.easy;

public class IntegerToString {

	public static void main(String[] args) {
		System.out.println(intToString(8, 10));
	}
	private static String intToString(int n, int base) { 
	    if (n == 0) 
	    	return "0";
	    StringBuilder sb = new StringBuilder();
	    while (n > 0) { 
	        int curr = n % base;
	        n = n/base;
	        sb.append(curr);
	    }
	    
	    char[] try1 = sb.substring(0).toCharArray(); 

	    StringBuilder result = new StringBuilder();
	    for (int i = try1.length-1; i>=0; i--) {
	        result.append(try1[i]); 
	    } 
	    
	    return result.toString();
	}
}


