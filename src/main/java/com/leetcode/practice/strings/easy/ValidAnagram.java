/*
Given two strings s and t , write a function to determine 
if t is an anagram of s.

Example 1:

Input: s = "anagram", t = "nagaram"
Output: true
Example 2:

Input: s = "rat", t = "car"
Output: false
Note:
You may assume the string contains only lowercase alphabets.

Follow up:
What if the inputs contain unicode characters? 
How would you adapt your solution to such case?
"anagram"
"nagaram"
*/

package com.leetcode.practice.strings.easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ValidAnagram {
	public static void main(String[] args) {
		System.out.println(ValidAnagram.isAnagram("bb",
				"aa"));
	}
	
	private static boolean isAnagram(String s, String t) {
		if(s.length() != t.length())
			return false;
		Map<Character, Integer> storage = new HashMap<>();
        char[] charArr1 = s.toCharArray();
        char[] charArr2 = t.toCharArray();
        Arrays.sort(charArr1);
        Arrays.sort(charArr2);
        for(int i=0; i < charArr1.length; i++) {
        	if(charArr1[i]!=charArr2[i]) {
        		return false;
        	}

        }       
        return true;
    }
	
//	private static boolean isAnagram(String s, String t) {
//		if(s.length() != t.length())
//			return false;
//		Map<Character, Integer> storage = new HashMap<>();
//        char[] charArr = s.toCharArray();
//        int result = 0;
//        int val = 0;
//        for(int i=0; i < charArr.length; i++) {
//        	if(storage.containsKey(charArr[i])) {
//        		val = storage.get(charArr[i]);
//        		storage.put(charArr[i], ++val);
//        	}
//        	else {
//        		storage.put(charArr[i], 1);
//        	}
//        }
//        charArr = t.toCharArray();
//        for(int j=0; j < charArr.length; j++) {
//        	if(storage.containsKey(charArr[j])) {
//        		val = storage.get(charArr[j]);
//        		if(val == 1) {
//        			storage.remove(charArr[j]);
//        		}
//        		else {
//        			storage.put(charArr[j], --val);
//        		}
//        		
//        	}
//        	else {
//        		return false;
//        	}
//        }
//       
//        return storage.isEmpty();
//    }
}
