/*
Write a function to find the longest common prefix string 
amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
Note:

All given inputs are in lowercase letters a-z.

*/

package com.leetcode.practice.strings.easy;

public class LongestCommonPrefix {

	public static void main(String[] args) {
		System.out.println(longestCommonPrefix(new String[]
				{"flower","flight", "flow"}));
	}
	
	private static String longestCommonPrefix(String[] strs) {
		if(strs.length == 0)
			return "";
        StringBuilder curr = new StringBuilder(strs[0]);
        StringBuilder prev = new StringBuilder();
        int pl = curr.length();
        for(int i=1; i < strs.length; i++) {
        	pl = curr.length();
        	prev = curr;
    		curr = new StringBuilder();
        	for(int j=0; j < strs[i].length(); j++) {
            	if(j > pl-1) {
            		break;
            	}
            	if(prev.charAt(j) == strs[i].charAt(j)) {
            		curr.append(prev.charAt(j));
            	}
            	else {
            		break;
            	}
        	}
        	
        }
		return curr.toString();
    }

}

