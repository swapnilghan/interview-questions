/*
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only store 
integers within the 32-bit signed integer range: 
[−2^31,  2^31 − 1]. 
For the purpose of this problem, assume that your function 
returns 0 when the reversed integer overflows.
 
*/

package com.leetcode.practice.strings.easy;

public class ReverseInteger {
	public static void main(String[] args) {
		System.out.println(ReverseInteger.reverse(9));
	}
	
	private static int reverse(int x) {
		boolean isModSet = false;
		if(x==0 || x > Integer.MAX_VALUE || x <Integer.MIN_VALUE) {
			return 0;
		}
		
		if(x<0) {
			isModSet = true;
			x = Math.abs(x);
		}
			
        StringBuilder digit = new StringBuilder();
		while(true) {
        	digit = digit.append(x % 10);
        	x = x / 10;
        	if(x<10) {
        		if(x!=0)
        			digit = digit.append(x);
        		break;
        	}
        }
		int answer = 0;
		try {
			answer = Integer.parseInt(digit.toString());
		}
		catch(NumberFormatException e) {
			return 0;
		}
		if(isModSet)
			answer = 0 - answer;
		return answer;
    }
}
