/**
Given a string, determine if it is a palindrome, 
considering only alphanumeric characters and ignoring cases.

Note: For the purpose of this problem, 
we define empty string as valid palindrome.

Example 1:

Input: "A man, a plan, a canal: Panama"
Output: true
Example 2:

Input: "race a car"
Output: false
*/

package com.leetcode.practice.strings.easy;

public class ValidPalindrome {

	public static void main(String[] args) {
		System.out.println(ValidPalindrome.
				isPalindrome("A man, a plan, a canal: Panama"));

	}

	private static boolean isPalindrome(String s) {
		if(s.length() == 0)
			return true;
        char[] arr = s.toCharArray();
        int i = 0;
        int j = arr.length-1;
        while(i < arr.length -1) {
        	if(Character.isLetterOrDigit(arr[i])) {
        		if(Character.isLetterOrDigit(arr[j])) {
        			if(Character.toLowerCase(arr[i]) 
            				!= Character.toLowerCase(arr[j]))
            			return false;
        			else {
        				i++;
        				j--;
        			}
        		}
        		else {
        			j--;
        		}
        	}
        	else {
        		i++;
        	}
        }
        return true;
    }
}
