/*

The count-and-say sequence is the sequence of integers with 
the first five terms as following:

1.     1
2.     11
3.     21
4.     1211
5.     111221

1 is read off as "one 1" or 11.
11 is read off as "two 1s" or 21.
21 is read off as "one 2, then one 1" or 1211.

Given an integer n where 1 ≤ n ≤ 30, generate the nth term of 
the count-and-say sequence.

Note: Each term of the sequence of integers will be represented 
as a string.

*/

package com.leetcode.practice.strings.easy;


public class CountAndSay {

	public static void main(String[] args) {
		System.out.println(CountAndSay.countAndSay(6));

	}

	private static String countAndSay(int n) {
        StringBuilder curr = new StringBuilder("1");
        StringBuilder prev = new StringBuilder();
        char say = ' ';
        int freq = 1;
        for(int i = 1; i < n; i++) {
        	prev = curr;
        	curr = new StringBuilder();
        	freq = 0;
        	say = prev.charAt(0);
        	
        	for(int j = 0; j < prev.length(); j++) {
        		if(say != prev.charAt(j)) {
        			curr.append(freq).append(say);
        			say = prev.charAt(j);
        			freq = 1;
        		}
        		else
        			freq++;
        	}
        	curr.append(freq).append(say);
        }
		return curr.toString();
    }
}
