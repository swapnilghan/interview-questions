package com.leetcode.practice.strings.easy;

public class PrintAllSubstringsofAString {

	public static void main(String[] args) {
		printAllSubstrings("swapnil");

	}

	private static void printAllSubstrings(String s) {
		for(int i=0; i < s.length(); i++) {
			for(int j = i+1; j <= s.length(); j++) {
				System.out.println(s.substring(i,j)+" - length - "+(j-i));
			}
		}
	}
}
