/*

Implement strStr().

Return the index of the first occurrence of needle in haystack, 
or -1 if needle is not part of haystack.

Example 1:

Input: haystack = "hello", needle = "ll"
Output: 2
Example 2:

Input: haystack = "aaaaa", needle = "bba"
Output: -1
Clarification:

What should we return when needle is an empty string? 
This is a great question to ask during an interview.

For the purpose of this problem, we will return 0 when needle 
is an empty string. This is consistent to C's strstr() and Java's indexOf().

*/

package com.leetcode.practice.strings.easy;

public class FirstOccurrence {

	public static void main(String[] args) {
		System.out.println(FirstOccurrence.strStr("mississippi",
				"issipi"));

	}
	
	private static int strStr(String haystack, String needle) {
		for(int i=0; i<haystack.length();i++) {
			for(int j=0; j<needle.length();i++) {

			}
		}
		return -1;
    }

}
