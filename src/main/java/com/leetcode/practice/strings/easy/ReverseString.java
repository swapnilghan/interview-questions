/*

Write a function that reverses a string. 
The input string is given as an array of characters char[].

Do not allocate extra space for another array, 
you must do this by modifying the input array in-place 
with O(1) extra memory.

You may assume all the characters consist of printable 
ascii characters.
 
Example 1:

Input: ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]

Example 2:

Input: ["H","a","n","n","a","h"]
Output: ["h","a","n","n","a","H"]
 
  
*/
package com.leetcode.practice.strings.easy;

public class ReverseString {
	public static void main(String[] args) {
		ReverseString.reverseString(new char[]
				{'h','e','l','l','o','s'});
	}
	
	private static void reverseString(char[] s) {
        int j = s.length - 1;
        char temp = ' ';
		for(int i=0; i < s.length/2; i++) {
			temp = s[i];
			s[i] = s[j];
			s[j] = temp;
        	j--;
        }
    }
}
