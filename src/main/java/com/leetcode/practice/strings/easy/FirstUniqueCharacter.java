/*
  
Given a string, find the first non-repeating character in it 
and return it's index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode",
return 2.
Note: You may assume the string contain only lowercase letters.

*/

package com.leetcode.practice.strings.easy;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class FirstUniqueCharacter {

	public static void main(String[] args) {
		System.out.println(FirstUniqueCharacter.
				firstUniqChar("loveleetcode"));

	}

	private static int firstUniqChar(String s) {
		char[] charArr = s.toCharArray();
		Set<Character> lookup = new HashSet<>();
		Map<Character, Integer> storage = new LinkedHashMap<>();
		for(int i=0; i < charArr.length; i++) {
			if(lookup.contains(charArr[i])){
				if(storage.containsKey(charArr[i])) {
					storage.remove(charArr[i]);
				}
			}
			else {
				lookup.add(charArr[i]);
				storage.put(charArr[i], i);
			}
		}
		
		int index = -1;
		if(storage.isEmpty())
			return index;
		else
			return storage.entrySet().iterator().next().getValue();
    }
}
