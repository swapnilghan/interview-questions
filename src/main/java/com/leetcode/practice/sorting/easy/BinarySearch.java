/*
Simple binary search program. 
*/
package com.leetcode.practice.sorting.easy;

import java.util.stream.IntStream;

public class BinarySearch {

	public static void main(String[] args) {
		int[] a = IntStream.range(0, 100).toArray();
		System.out.println(runBinarySearchIteratively(a, 72));
	}

	private static int runBinarySearchIteratively(int[] sortedArray, int key) {
		int index = Integer.MAX_VALUE;
		int low = 0;
		int high = sortedArray.length;

		while (low <= high) {
			int mid = (low + high) / 2;
			if (sortedArray[mid] < key) {
				low = mid + 1;
			} else if (sortedArray[mid] > key) {
				high = mid - 1;
			} else if (sortedArray[mid] == key) {
				index = mid;
				break;
			}
		}
		return index;
	}
}
