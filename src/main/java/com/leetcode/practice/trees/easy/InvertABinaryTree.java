package com.leetcode.practice.trees.easy;
/**

@author swghan
Invert a binary tree.

Example:

Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9
Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1

*/

public class InvertABinaryTree {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(4);
		TreeNode left = new TreeNode(2);
		TreeNode right = new TreeNode(7);
		TreeNode three = new TreeNode(1);
		TreeNode four = new TreeNode(3);
		TreeNode five = new TreeNode(6);
		TreeNode six = new TreeNode(9);
		root.left = left;
		root.right = right;
		left.left = three;
		left.right = four;
		right.left = five;
		right.right = six;
		System.out.println(invertTree(root));
		System.out.println("here");
	}
	
	private static TreeNode invertTree(TreeNode root) {
        if(root == null)
        	return null;
        
        recursiveInvert(root);
        
        return root;
    }

	private static void recursiveInvert(TreeNode node) {
		if(node == null)
			return;
		
		if(node.left == null && node.right== null)
			return;
		
		TreeNode temp = node.left;
		node.left = node.right;
		node.right = temp;
		
		recursiveInvert(node.left);
		recursiveInvert(node.right);
		
	}
}
