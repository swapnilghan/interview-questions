/**
Given a binary tree, check whether it is a mirror of itself 
(ie, symmetric around its center).

For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3
 

But the following [1,2,2,null,3,null,3] is not:

    1
   / \
  2   2
   \   \
   3    3
 */
package com.leetcode.practice.trees.easy;

/*
Definition for a binary tree node.
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}
*/

public class SymmetricTrees {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		TreeNode left = new TreeNode(2);
		TreeNode right = new TreeNode(2);
		TreeNode three = new TreeNode(3);
		TreeNode four = new TreeNode(4);
		TreeNode five = new TreeNode(4);
		TreeNode six = new TreeNode(3);
		root.left = left;
		root.right = right;
		left.left = three;
		left.right = four;
		right.left = five;
		right.right = six;
		
		System.out.println(isSymmetric(root));

	}
	
	private static boolean isSymmetric(TreeNode root) {
        if(root == null)
        	return true;
        
        return recursivecheck(root, root);
		
    }
	
	private static boolean recursivecheck(TreeNode t1, TreeNode t2) {
		if(t1 == null && t2 == null)
			return true;
		
		if(t1 !=null && t2 != null && t1.val == t2.val) {
			return(recursivecheck(t1.left, t2.right) 
					&& recursivecheck(t1.right, t2.left));
		}
		return false;
	}

}
