package com.leetcode.practice.trees.medium;

import java.util.LinkedList;
import java.util.Queue;

/**
Given a binary tree, determine if it is a complete binary tree.

Definition of a complete binary tree from Wikipedia:
In a complete binary tree every level, except possibly the last, is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.

 

Example 1:

    1
   / \
  2   3
 / \ / \
4  5 6  
 

Input: [1,2,3,4,5,6]
Output: true
Explanation: Every level before the last is full (ie. levels with node-values {1} and {2, 3}), and all nodes in the last level ({4, 5, 6}) are as far left as possible.


Example 2:

    1
   / \
  2   3
 / \   \
4  5    7  
 

Input: [1,2,3,4,5,null,7]
Output: false
Explanation: The node with value 7 isn't as far left as possible.

*/

import com.leetcode.practice.trees.easy.TreeNode;


public class CheckCompleteBinaryTree {
	public static void main(String[] args) {
		TreeNode root = TreeNode.stringToTreeNode("[1,2,null,3]");
		System.out.println(isCompleteTree(root));
	}
	
	public static boolean isCompleteTree(TreeNode root) {
		if(root == null)
			return true;
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		boolean isComplete = false;
		queue.add(root);
		TreeNode examine;
		boolean nullOcc = false;
		while(!queue.isEmpty()) {
			examine = queue.poll();
			if(examine == null) {
				nullOcc = true;
				if(queue.isEmpty()) {
					return true;
				}		
			}
			else {
				if(nullOcc)
					return false;
				queue.offer(examine.left);
				queue.offer(examine.right);
			}	
		}
		return isComplete;
	}

}
