package com.leetcode.practice.arrays.medium;

/*

Given an array of n positive integers and a positive integer s, 
find the minimal length of a contiguous subarray of which the 
sum ≥ s. If there isn't one, return 0 instead.

Example: 

Input: s = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: the subarray [4,3] has the minimal length under 
the problem constraint.
Follow up:
If you have figured out the O(n) solution, try coding 
another solution of which the time complexity is O(n log n). 

*/

public class MinSizeSubarraySum {

	public static void main(String[] args) {
		System.out.println(minSubArrayLen(110, new int[] {1,2,3,4,5}));
		//System.out.println(minSubArrayLen(7, new int[] {2,3,1,2,4,3}));
	}
	
	private static int minSubArrayLen(int s, int[] nums) {
        int size = 0;
        int answer = Integer.MAX_VALUE;
        int sum = 0;
        for(int i=0, j=0; i < nums.length; i++) {
        	sum = sum + nums[i];
        	size++;
        	while(sum >= s && j < nums.length) {
        		answer = Math.min(size, answer);
        		sum = sum - nums[j++];
        		size--;
        	}
        }

        return answer == Integer.MAX_VALUE ? 0 : answer;
    }

}
