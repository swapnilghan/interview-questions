
package com.leetcode.practice.arrays.easy;

import java.util.HashSet;
import java.util.Set;

public class ContainsDuplicate {

	public static void main(String[] args) {

		RotateArray.containsDuplicate(new int[]{1,2,3,1});
	}
	
	private static boolean containsDuplicate(int[] nums) {
		Set<Integer> hs = new HashSet<>();
		for(int i=0; i < nums.length; i++) {
			if(!hs.add(nums[i]))
				return false;
		}
		return true;
    }

}

