/*

Given two arrays, write a function to compute their intersection.

Example 1:

Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2,2]
Example 2:

Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [4,9]
Note:

Each element in the result should appear as many times as it shows 
in both arrays. The result can be in any order.
Follow up:

What if the given array is already sorted? How would you optimize your algorithm?
What if nums1's size is small compared to nums2's size? Which algorithm is better?
What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?

*/

package com.leetcode.practice.arrays.easy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntersectionOfTwoArrays {

	public static void main(String[] args) {
		IntersectionOfTwoArrays.intersect(new int[] {1,2,2,1}, 
				new int[] {2,2});
	}

    private static int[] intersect(int[] nums1, int[] nums2) {
        int pt1=0;
        int pt2=0;
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> result = new ArrayList<>();
    	while(pt1 < nums1.length && pt2 < nums2.length) {
        	if(nums1[pt1] ==  nums2[pt2]) {
        		result.add(nums1[pt1]);
        		pt1++;
        		pt2++;
        	}
        	else if(nums1[pt1] >  nums2[pt2]) {
        		pt2++;
        	}
        	else {
        		pt1++;
        	}
        }
    	int[] ans = new int[result.size()];
    	for(int a =0; a < result.size(); a++)
    		ans[a] = result.get(a);
    	return ans;
    }
}
