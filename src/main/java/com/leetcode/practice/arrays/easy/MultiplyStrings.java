/*

Given two non-negative integers num1 and num2 
represented as strings, return the product of num1 and num2, 
also represented as a string.

Example 1:

Input: num1 = "2", num2 = "3"
Output: "6"
Example 2:

Input: num1 = "123", num2 = "456"
Output: "56088"
Note:

The length of both num1 and num2 is < 110.
Both num1 and num2 contain only digits 0-9.
Both num1 and num2 do not contain any leading zero, except the number 0 itself.
You must not use any built-in BigInteger library or convert the inputs to integer directly.

*/

package com.leetcode.practice.arrays.easy;

import java.math.BigInteger;

public class MultiplyStrings {
	public static void main(String[] args) {
		System.out.println("1".charAt(0)-'0');
	}
	
	private static String multiply(String num1, String num2) {
		BigInteger result = new BigInteger(num1)
		.multiply(new BigInteger(num2));
		return result.toString();
        
    }
}
