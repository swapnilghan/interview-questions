/*
 
You are a professional robber planning to rob houses along a street. 
Each house has a certain amount of money stashed, the only constraint 
stopping you from robbing each of them is that adjacent houses have 
security system connected and it will automatically contact the 
police if two adjacent houses were broken into on the same night.

Given a list of non-negative integers representing the amount of money 
of each house, determine the maximum amount of money you can rob 
tonight without alerting the police.

Example 1:

Input: [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
             Total amount you can rob = 1 + 3 = 4.
Example 2:

Input: [2,7,9,3,1]
Output: 12
Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and 
rob house 5 (money = 1).
             Total amount you can rob = 2 + 9 + 1 = 12.

*/

package com.leetcode.practice.arrays.easy;

public class HouseRobber {

	public static void main(String[] args) {
		System.out.println(rob(new int[] {2,7,9,3,1}));
		System.out.println(rob(new int[] {2,1,1,2}));
	}
	
	private static int rob1(int[] nums) {
        int sum1 = 0;
        int sum2 = 0;
        int i = 0, j = 1;
        while(i < nums.length || j < nums.length) {
        	if(i < nums.length)
        		sum1 = sum1 + nums[i];
        	if(j < nums.length)
        		sum2 = sum2 + nums[j];
        	i += 2;
        	j += 2;
        }
        return sum1 > sum2 ? sum1 : sum2;
    }
	
	private static int rob(int[] nums) {
		if(nums.length == 0)
			return 0;
		if(nums.length == 1)
			return nums[0];
		
        int sum1 = nums[0];
        int sum2 = nums[1];
        int v1 =0,v2 = 0;
        int i = 0, j = 1;
        
        while(i < nums.length && ((i+2)<nums.length || (i+3)<nums.length)) {
    		if(i+2 < nums.length) {
    			v1 = nums[i+2];
    		}
    		if(i+3 < nums.length) {
    			v2 = nums[i+3];
    		}
    		if(v1 > v2) {
    			sum1 = sum1 + v1;
    			i=i+2;
    		}
    		else {
    			sum1 = sum1 + v2;
    			i=i+3;
    		}
    		v1=0;v2=0;
        }
        
    	while(j < nums.length && ((j+2)<nums.length || (j+3)<nums.length)) {
    		if(j+2 < nums.length) {
    			v1 = nums[j+2];
    		}
    		if(j+3 < nums.length) {
    			v2 = nums[j+3];
    		}
    		if(v1 > v2) {
    			sum2 = sum2 + v1;
    			j=j+2;
    		}
    		else {
    			sum2 = sum2 + v2;
    			j=j+3;
    		}
    		v1=0;v2=0;
    	}
        
        return sum1 > sum2 ? sum1 : sum2;
    }

}
