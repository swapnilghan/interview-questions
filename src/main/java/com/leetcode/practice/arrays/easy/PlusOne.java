/*

Given a non-empty array of digits representing a non-negative 
integer, plus one to the integer.

The digits are stored such that the most significant digit 
is at the head of the list, and each element in the array contain 
a single digit.

You may assume the integer does not contain any leading zero, 
except the number 0 itself.

Example 1:

Input: [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Example 2:

Input: [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.

*/

package com.leetcode.practice.arrays.easy;

public class PlusOne {

	public static void main(String[] args) {
		PlusOne.plusOne(new int[] {9,9,9,9});

	}
	
	private static int[] plusOne(int[] digits) {
        int carry = 0;
        int sum = 0;
		for(int i=digits.length-1; i>=0; i--) {
			if(i == digits.length-1)
				sum = digits[i] + 1 + carry;
			else
				sum = digits[i] + carry;
			carry = 0;
			if(sum >= 10) {
				sum = sum-10;
				carry = 1;
			}
			digits[i] = sum;
			if(carry == 0)
				return digits;
		}
		if(carry==1) {
			int newlen = digits.length;
			int[] result = new int[++newlen];
			result[0] = 1;
			int j = 0;
			for(int i=1; i < result.length; i++) {
				result[i]=digits[j];
				j++;
			}
			digits = result;
		}
		return digits;
    }

}
