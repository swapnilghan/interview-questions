/*
 Given two sorted integer arrays nums1 and nums2, merge nums2 
 into nums1 as one sorted array.

Note:

The number of elements initialized in nums1 and nums2 are m and n respectively.
You may assume that nums1 has enough space (size that is greater 
or equal to m + n) to hold additional elements from nums2.
Example:

Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

Output: [1,2,2,3,5,6]
 
*/

package com.leetcode.practice.arrays.easy;

public class MergeTwoSortedArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums1 = new int[] {1,3,5,0,0,0};
		int[] nums2 = new int[] {2,4,6};
		MergeTwoSortedArrays.merge(nums1, 3, nums2, 3);
	}

	//without using additional space.
	//assuming first array will have space for all the integers.
	public static void merge(int[] nums1, int m, int[] nums2, int n) {
        if(n == 0)
        	return;
        int k = (m + n)-1;
        m--;n--;
        //start filling array from the end.
        for(int i = k; i>=0; i--) {
        	if(m < 0)
        		nums1[i] = nums2[n--];
        	else if(n < 0)
        		nums1[i] = nums1[m--];
        	else if(nums1[m] >= nums2[n])
        		nums1[i] = nums1[m--];
        	else if(nums2[n] > nums1[m]) 
        		nums1[i] = nums2[n--];
        }
    }
	
	//merge two arrays into a single array. newly created array will be returned.
	private static int[] merge1(int[] a, int[] b) {
		int[] answer = new int[a.length+b.length];
		
		int p1 = 0, p2 = 0;
		int index = 0;
		while(p1 != a.length || p2 != b.length) {
			if(p1 == a.length) {
				answer[index++] = b[p2];
				p2++;
			}
			else if(p2 == b.length) {
				answer[index++] = a[p1];
				p1++;
			}
			if(p2 != b.length && a[p1] >= b[p2]) {
				answer[index++] = b[p2];
				p2++;
			}
			else if (p1 != a.length) {
				answer[index++] = a[p1];
				p1++;
			}
		}
		return answer;
	}
	
}
