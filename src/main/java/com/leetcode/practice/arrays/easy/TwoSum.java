/*

Given an array of integers, return indices of the two 
numbers such that they add up to a specific target. 
You may assume that each input would have exactly one 
solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

*/

package com.leetcode.practice.arrays.easy;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {
	
	public static void main(String[] args) {
		TwoSum object  =  new TwoSum();
		int[] nums = new int[] {2,7,11,13,15};
		int[] ans = object.twoSum_my_optimal(nums, 28);
		System.out.println(ans[0]+","+ans[1]);
	}
	
    public int[] twoSum_default(int[] nums, int target) {
        int len = nums.length;
        int[] answer = new int[2];

        for(int i=0; i < len; i++) {
        	for(int j=i+1; j < len; j++) {
        		if(nums[i]+nums[j] == target) {
        			answer[0]=i;
        			answer[1]=j;
        			return answer;
        		}
        	}
        }
		throw new IllegalArgumentException("No two sum solution exists");
    }
    
    public int[] twoSum_my_optimal(int[] numbers, int target) {
    	int[] answer = new int[2];
    	Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    	for(int i=0; i < numbers.length; i++) {
    		if(map.containsKey(target - numbers[i])) {
    			answer[0]=map.get(target- numbers[i]);
    			answer[1]=i;
    		}
    		map.put(numbers[i], i);
    	}
    	return answer;
    }
    
    public int[] twoSum_accepted(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement)) {
                return new int[] { map.get(complement), i };
            }
            map.put(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}

