package com.leetcode.practice.arrays.easy;

public class SingleDuplicate {
	public static void main(String[] args) {

		System.out.println(SingleDuplicate.findOneDuplicate
				(new int[]{1,2,3,1}));
	}
	
	private static int findOneDuplicate(int[] nums) {
		int dup = 0;
		for(int i=0; i < nums.length; i++) {
			dup = dup ^ nums[i];
		}
		return dup;
    }
}
