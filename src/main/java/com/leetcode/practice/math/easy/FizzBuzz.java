/*

Write a program that outputs the string representation of numbers 
from 1 to n.

But for multiples of three it should output “Fizz” instead 
of the number and for the multiples of five output “Buzz”. 
For numbers which are multiples of both three and 
five output “FizzBuzz”.

Example:

n = 15,

Return:
[
    "1",
    "2",
    "Fizz",
    "4",
    "Buzz",
    "Fizz",
    "7",
    "8",
    "Fizz",
    "Buzz",
    "11",
    "Fizz",
    "13",
    "14",
    "FizzBuzz"
]

*/

package com.leetcode.practice.math.easy;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

	public static void main(String[] args) {
		List<String> answer = FizzBuzz.fizzBuzzWithoutMod(15);
		for(String val : answer)
			System.out.println(val);

	}
	
	private static List<String> fizzBuzz(int n) {
        List<String> answer = new ArrayList<String>();

        for(int i=1; i<=n; i++) {
        	answer.add(i % 3 ==0 ? (i % 5 == 0 ? "FizzBuzz" : "Fizz") : 
        		i % 5 == 0 ? "Buzz" : String.valueOf(i));
        }
        return answer;
    }

	private static List<String> fizzBuzzWithoutMod(int n) {
        List<String> answer = new ArrayList<String>();
        int fizz = 0;
        int buzz = 0;
        int fizzbuzz = 0;
        for(int i=1; i<=n; i++) {
        	fizz++;
        	buzz++;
        	fizzbuzz++;
        	if(fizz == 3) {
        		fizz = 0;
        		answer.add("Fizz");
        	}
        	else if(buzz == 5) {
        		buzz = 0;
        		answer.add("Buzz");
        	}
        	else if(fizzbuzz == 15) {
        		fizzbuzz = 0;
        		answer.add("FizzBuzz");
        	}
        	else {
        		answer.add(String.valueOf(i));
        	}
        }
        return answer;
    }
}
