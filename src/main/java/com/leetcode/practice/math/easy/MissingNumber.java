/*

Given an array containing n distinct numbers taken 
from 0, 1, 2, ..., n, find the one that is missing from the array.

Example 1:

Input: [3,0,1]
Output: 2
Example 2:

Input: [9,6,4,2,3,5,7,0,1]
Output: 8
Note:
Your algorithm should run in linear runtime complexity. 
Could you implement it using only constant extra space complexity?

*/

package com.leetcode.practice.math.easy;

public class MissingNumber {

	public static void main(String[] args) {
		System.out.println(missingNumber(new int[] {9,6,4,2,3,5,7,0,1}));
	}
	
	//sum of 1....n numbers is (n * (n+1)) /2
	private static int missingNumber(int[] nums) {
        int sum = (nums.length * (nums.length +1)) /2 ;
        for(int i : nums) {
        	sum = sum -i;
        }
        return sum;
    }

	//using xor operator
	public int missingNumber1(int[] nums) { //xor
	    int res = nums.length;
	    for(int i=0; i<nums.length; i++){
	        res ^= i;
	        res ^= nums[i];
	    }
	    return res;
	}
}
