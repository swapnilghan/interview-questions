/*
 
Count the number of prime numbers less than a non-negative number, n.

Example:

Input: 10
Output: 4
Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
 
*/

package com.leetcode.practice.math.easy;

import java.util.ArrayList;

public class CountPrimes {

	public static void main(String[] args) {
		System.out.println(countPrimesOptimum(500));

	}
	
	//Given an input number n, check whether any prime integer 
	//m from 2 to √n evenly divides n (the division leaves no remainder).
	private static int countPrimes(int n) {
		if(n <= 2)
			return 0;
		if(n == 3)
			return 1;
		
        ArrayList<Integer> primeList = new ArrayList<>();
        primeList.add(2);
        primeList.add(3);
        boolean isPrime = true;
        int innerVar = 0;
        for(int i = 5; i < n; i=i+2) {
        	isPrime = true;
        	innerVar = (int) Math.floor(Math.sqrt(i));
        	for(int j = 0;  j < innerVar; j++) {
        		if( i % primeList.get(j) == 0) {
        			isPrime = false;
        			break;
        		}
        	}
        	if(isPrime) {
    			primeList.add(i);
    		}
        }
		return primeList.size();
		
    }

	private static int countPrimesOptimum(int n) {		
        boolean[] notPrime = new boolean[n];
        int count = 0;
        for (int i = 2; i < n; i++) {
            if (notPrime[i] == false) {
                count++;
                for (int j = 2; i*j < n; j++) {
                    notPrime[i*j] = true;
                }
            }
        }
        return count;
    }
}
