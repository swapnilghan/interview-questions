/*

Given an integer, write a function to determine if it is a 
power of three.

Example 1:

Input: 27
Output: true
Example 2:

Input: 0
Output: false
Example 3:

Input: 9
Output: true
Example 4:

Input: 45
Output: false

Follow up:
Could you do it without using any loop / recursion?
 */

package com.leetcode.practice.math.easy;

public class PowerofThree {

	public static void main(String[] args) {
		System.out.println(PowerofThree.isPowerOpt(0));
	}

	private static boolean isPowerOfThree(int n) {
        int result = 1;
        while(!(result > n)) {
        	if(result == n) {
        		return true;
        	}
        	result = result * 3;
        	
        }
        return false;
    }
	
	private static boolean isPowerOpt(int n) {
		if(n == 0) return false;
		while (n % 3 == 0) {
		    n = n/3;
		}
		return n == 1;
	}
}
