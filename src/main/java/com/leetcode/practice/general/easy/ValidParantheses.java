/*

Given a string containing just the characters 
'(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.

Example 1:

Input: "()"
Output: true
Example 2:

Input: "()[]{}"
Output: true
Example 3:

Input: "(]"
Output: false
Example 4:

Input: "([)]"
Output: false
Example 5:

Input: "{[]}"
Output: true

*/

package com.leetcode.practice.general.easy;

import java.util.Stack;

public class ValidParantheses {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(ValidParantheses.isValid("]"));
	}
	
	private static boolean isValid(String s) {
		if(s.length() == 0 || s == null) {
			return true;
		}
        
        boolean isValid = true;
        Stack<Character> stack = new Stack<>();
        int i = 0;
        for(char c : s.toCharArray()) {
        	if(c == '(' || c == '[' || c == '{') {
        		stack.push(c);
        	}
        	else if(c == ')') {
        		if(stack.isEmpty() || stack.pop() != '(')
        			return false;
        	}
        	else if(c == '}') {
        		if(stack.isEmpty() || stack.pop() != '{')
        			return false;
        	}
        	else if(c == ']') {
        		if(stack.isEmpty() || stack.pop() != '[')
        			return false;
        	}
        	i++;
        }
        
        if(stack.isEmpty())
        	return isValid;
        return false;
    }

}
