/*

Given a string containing just the characters '(' and ')', 
find the length of the longest valid (well-formed) parentheses 
substring.

Example 1:

Input: "(()"
Output: 2
Explanation: The longest valid parentheses substring is "()"
Example 2:

Input: ")()())"
Output: 4
Explanation: The longest valid parentheses substring is "()()"

*/

package com.leetcode.practice.general.easy;

import java.util.Stack;

public class LongestValidParantheses {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(longestValidParentheses("()(()"));
	}
	
	private static int longestValidParentheses(String s) {
		if(s.length() == 0 || s == null) {
			return 0;
		}
        
        int validCount = 0;
        Stack<Character> stack = new Stack<>();
        for(char c : s.toCharArray()) {
        	if(c == '(' || c == '[' || c == '{') {
        		stack.push(c);
        	}
        	else if(c == ')') {
        		if(!(stack.isEmpty() || stack.pop() != '('))
        			validCount += 2;
        	}
        	else if(c == '}') {
        		if(stack.isEmpty() || stack.pop() != '{')
        			validCount += 2;
        	}
        	else if(c == ']') {
        		if(stack.isEmpty() || stack.pop() != '[')
        			validCount += 2;
        	}
        }
        
        return validCount;
    }
}
