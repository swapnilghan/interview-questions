/*
Find the number total bits that are set to 1 in a number.
*/
package com.leetcode.practice.bit_operations.easy;

public class oneBitsCount {

	public static void main(String[] args) {
		oneBitsCount object = new oneBitsCount();
		System.out.println(object.calculateOneBits_n(7879));
		System.out.println(object.calculateOneBits(787987999));

	}
	
	public int calculateOneBits_n(Integer num) {
		int count=0;
		for(int i=0;(1<<i) <= num;i++) {
			if((num & (1<<i)) > 0) {
				count++;
			}
		}
		return count;
	}
	public int calculateOneBits(Integer num) {
		System.out.println(Integer.toBinaryString(num));
		if(num == 0) {
			return 0;
		}
		
		int count = 0;
		int leastBit = 0;
		while(num > 0) {
			leastBit = num & ~(num-1);
			if(leastBit > 0) {
				count++;
			}
			num = num ^ leastBit;
		}
		return count;
	}
}
