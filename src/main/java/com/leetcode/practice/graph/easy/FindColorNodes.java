package com.leetcode.practice.graph.easy;

import java.util.HashSet;
import java.util.Set;

public class FindColorNodes {

	private static Set<String> neighbors = new HashSet<>();
	private static int[][] grid = new int[][]{{1,1,1,0,0},
		   {2,1,0,0,0},
		   {2,1,0,2,0},
		   {2,0,1,0,2},
		   {2,0,0,0,1}
		};
	//private static int[][] grid = new int[][]{{0}};
	private static	int len = grid[0].length;
	public static void main(String[] args) {

		int color = grid[1][0];
		FindColorNodes.calculateColorCount(1, 0, color);
		System.out.println(neighbors.size());
	}
	
	private static void calculateColorCount(int x, int y, int color) {
		if(x >= 0 && x <= len-1 && y >= 0 && y <= len-1) {
			neighbors.add(x+"*"+y);
			addNeighbor(x, y, color);
		}
	}
	
	private static void addNeighbor(int x, int y, int color) {
		boolean alreadyPresent = false;
		if(x >= 0 && x <= len-1 && y >= 0 && y < len-1) {
			if(!(x==0) && grid[x-1][y] == color) {
				alreadyPresent = neighbors.add(x-1+"*"+y);
				if(alreadyPresent)
					addNeighbor(x-1, y, color);
			}
			if(!(y==0) && grid[x][y-1] == color) {
				alreadyPresent = neighbors.add(x+"*"+(y-1));
				if(alreadyPresent)
					addNeighbor(x, y-1, color);
			}
			if(!(x==len-1) && grid[x+1][y] == color) {
				alreadyPresent = neighbors.add(x+1+"*"+y);
				if(alreadyPresent)
					addNeighbor(x+1, y, color);
			}
			if(!(y==len-1) && grid[x][y+1] == color) {
				alreadyPresent = neighbors.add(x+"*"+ (y+1));
				if(alreadyPresent)
					addNeighbor(x, y+1, color);
			}
		}
	}

}
