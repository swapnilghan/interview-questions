/*

Given a linked list, remove the n-th node from the end of 
list and return its head.

Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list 
becomes 1->2->3->5.

Note:

Given n will always be valid.

Follow up:

Could you do this in one pass?

*/

package com.leetcode.practice.linkedlist.easy;

import java.util.Stack;

public class RemoveNthFromEnd {

	public static void main(String[] args) {
		ListNode root = new ListNode(1);
		root.next = new ListNode(2);
		//root.next.next = new ListNode(3);
		//root.next.next.next = new ListNode(4);
		//root.next.next.next.next = new ListNode(5);
		//root.next.next.next.next.next = new ListNode(6);
		RemoveNthFromEnd.removeNthEnd(root, 2);
	}
	
	private static ListNode removeNthEnd(ListNode head, int n) {
		 ListNode start = new ListNode(0);
		 ListNode slow = start, fast = start;
		 slow.next = head;
		    
		  //Move fast in front so that the gap between slow and fast becomes n
		    for(int i=1; i<=n+1; i++)   {
		        fast = fast.next;
		    }
		    //Move fast to the end, maintaining the gap
		    while(fast != null) {
		       slow = slow.next;
		       fast = fast.next;
		    }
		    //Skip the desired node
		    slow.next = slow.next.next;
		    return start.next;
	}
	
	private static ListNode removeNthFromEnd(ListNode head, int n) {
		if(head == null || head.next == null)
			return null;
        ListNode current = head;
        Stack<ListNode> s = new Stack<>();
        
		while(current != null) {
        	s.push(current);
        	if(current.next != null) {
        		current = current.next;
        	}	
        	else
        		break;
        }
		int index = (s.size() - n);
		ListNode from = null;
		
		if(index == 0)
			return head.next;
		
		from = s.get(index-1);
		
		ListNode to = null;
		if(!(index == (s.size()-1)))	
			to = s.get(index+1);
		from.next = to;
		return head;
    }

	private static void loopOverLinkedlist(ListNode head) {
		ListNode current = head;
		while(current != null) {
        	System.out.println(current.val);
        	if(current.next != null)
        		current = current.next;
        	else
        		break;
        }
	}
}

