/*
 * Reverse a singly linked list.

Example:

Input: 1->2->3->4->5->NULL
Output: 5->4->3->2->1->NULL
Follow up:

A linked list can be reversed either iteratively or recursively. 
Could you implement both?
 
*/

package com.leetcode.practice.linkedlist.easy;

public class ReverseLinkedList {
	public static void main(String[] args) {
		ListNode root = new ListNode(1);
		root.next = new ListNode(2);
		root.next.next = new ListNode(3);
		root.next.next.next = new ListNode(4);
		root.next.next.next.next = new ListNode(5);
		ReverseLinkedList.reverseListRecursive(root);
	}
	
	
	private static ListNode reverseList(ListNode head) {
	    ListNode prev = null; 
	    ListNode current = head;
	    ListNode next = null;
	    while(current != null) {
	    	next = current.next;
	    	current.next = prev;
	    	prev = current;
	    	current = next;
	    }
		return prev;
	}
	
	private static ListNode reverseListRecursive(ListNode head) {
		if(head == null) {
			return head;
	    }
	
		return reverseListRecursive(head.next);
	}

	
}
