/*
 
Merge two sorted linked lists and return it as a new list. 
The new list should be made by splicing together the nodes of the 
first two lists.

Example:

Input: 1->2->4, 1->3->4
Output: 1->1->2->3->4->4
 
*/

package com.leetcode.practice.linkedlist.easy;

public class MergeSortedLinkedLists {

	public static void main(String[] args) {
		ListNode root = new ListNode(1);
		root.next = new ListNode(2);
		root.next.next = new ListNode(4);
		
		ListNode root1 = new ListNode(2);
		root.next = new ListNode(4);
		root.next.next = new ListNode(4);
		
		MergeSortedLinkedLists.mergeTwoLists(root, root1);
	}

	private static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode head1 = l1; ListNode head2 = l2;
        ListNode next;
        while(head2 != null) {
        	if(head1.val > head2.val) {
        		head1 = head1.next;
        	}
        }
        
		return null;
    }
	
	private static ListNode mergeTwoListsRecursive(ListNode l1, ListNode l2){
		if(l1 == null) return l2;
		if(l2 == null) return l1;
		if(l1.val < l2.val){
			l1.next = mergeTwoLists(l1.next, l2);
			return l1;
		} else{
			l2.next = mergeTwoLists(l1, l2.next);
			return l2;
		}
}
}
