/*
You are given two non-empty linked lists representing 
two non-negative integers. The digits are stored in 
reverse order and each of their nodes contain a single 
digit. Add the two numbers and return it as a linked list.
You may assume the two numbers do not contain 
any leading zero, except the number 0 itself.

Example:

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.

*/

package com.leetcode.practice.linkedlist.medium;


public class AddTwoNumbers {
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {    
    	ListNode result = new ListNode(0);
    	int car = 0;
    	int sum = 0;
    	int n1 = 0;
    	int n2 = 0;
    	ListNode node =  result;
        while(l1 != null || l2 != null) {
            if(l1 != null) {
            	n1 = l1.val;
            	l1 = l1.next;
            }
            if(l2 != null) {
            	n2 = l2.val;
            	l2 = l2.next;
            }
            sum = n1+n2+car;
        	car=0;
        	if(sum > 9) {
        		sum = sum - 10;
        		car = 1;
        	}
            node.next = new ListNode(sum);
            node = node.next;
            sum = 0;
            n1 = 0;
            n2 = 0;
        }
        if(car == 1) {
        	node.next = new ListNode(1);
        }
        return result.next;
    }
    
	  private static ListNode addTwoNumbers_o(ListNode l1, ListNode l2) {
	      ListNode head = null;
	      ListNode current = null;
	      ListNode a = l1;
	      ListNode b = l2;
	      int carry = 0;
	      while(a != null || b != null || carry != 0) {
	          int sum = carry;
	          sum += a != null ? a.val : 0;
	          sum += b != null ? b.val : 0;
	          carry = sum / 10;
	          if (head == null) {
	              head = new ListNode(sum % 10);
	              current = head;
	          } else {
	              current.next = new ListNode(sum % 10);
	              current = current.next;
	          }
	          a = a != null? a.next: null;
	          b = b != null? b.next: null;
	      }
	      return head;
	  }
    public static void main(String[] args) {
    	ListNode l1 = new ListNode(2);
    	l1.next = new ListNode(4);
    	l1.next.next = new ListNode(3);
    	//l1.next.next.next = new ListNode(9);
    	ListNode l2 = new ListNode(5);
    	l2.next = new ListNode(6);
    	l2.next.next = new ListNode(4);
    	AddTwoNumbers.addTwoNumbers(l1, l2);
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}